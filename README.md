# Compressive ICA Package

Papers associated with this code:

1. "Compressive Independent Component Analysis" IEEE 2019 27th European Signal Processing Conference (EUSIPCO) - https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8903095

2. "Compressive Independent Component Analysis: Theory and Algorithms" - https://arxiv.org/pdf/2110.08045.pdf


## Requirements

* MATLAB (tested on 2019b (and later))
* Tensorlab Toolbox (See https://www.tensorlab.net/)

